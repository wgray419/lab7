// Programmer:	Wyatt Gray
// Clemson username:  wrgray
// Lab section:	 Section 4
// Lab #:  Lab 6
// Name of TA:  Elliot & Franchi

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {

  //This is to seed the random generator

  srand(unsigned (time(0)));


  /*Creates an array of 10 employees and fills it with information 
  from standard input with prompt messages*/

  employee information[10];
  for(int i = 0; i < 10; i++)
  {
    cout << "What is employee number " << i + 1 << "'s first name?" << endl;
    cin >> information[i].firstName;
    cout << "what is employee number " << i + 1 << "'s last name?" << endl;
    cin >> information[i].lastName;
    cout << "What year was employee " << i + 1 << " born?" << endl;
    cin >> information[i].birthYear;
    cout << "what is employee " << i + 1 << "'s hourly wage?" << endl;
    cin >> information[i].hourlyWage;
    cout << endl;
  }

  //After the array is created and initialzed we call random_shuffle()

  random_shuffle (&information[0], &information[10], &myrandom);

  /*Builds a smaller array of 5 employees from the first five cards 
  of the array created above*/

  employee print[5] = { information[0], information[1], information[2], 
  information[3], information[4] };

   //Sorts the new array.
   sort(&print[0], &print[5], name_order);

   //Now prints the array below
   for(int i = 0; i < 5; i++)
   {
     cout << "Employee " << i + 1 << ":" << endl;

     cout << setw(18) << right << print[i].lastName +", "+ 
          print[i].firstName << endl;

     cout << setw(18) << right << print[i].birthYear << endl;

     cout << setw(18) << right << setprecision(2) << fixed << showpoint
          << print[i].hourlyWage << endl;

     cout << endl;
   } 

  // End of program.
  return 0;
}


//This function will be passed to the sort funtion.
//Puts the names in alphabetical order.
bool name_order(const employee& lhs, const employee& rhs) 
{
  if(lhs.lastName < rhs.lastName)
    return true;
  else
    return false;
}

